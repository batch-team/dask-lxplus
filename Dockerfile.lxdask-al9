FROM gitlab-registry.cern.ch/linuxsupport/alma9-base
LABEL authors="b.jones@cern.ch"

RUN curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-linux-x86_64.sh" \
    && mv Miniforge3-linux-x86_64.sh /tmp/miniconda.sh
ADD docker-files/batch9-stable.repo /etc/yum.repos.d/
RUN yum -y update \
    && yum -y install curl which bzip2 libgfortran git file man-db \
    && bash /tmp/miniconda.sh -bfp /usr/local/ \
    && rm -f /tmp/miniconda.sh \
    && conda update conda \
    && conda clean --all --yes \
    && yum clean all

# CERN batch submission bits
RUN yum -y install myschedd krb5-workstation ngbauth-submit
ADD docker-files/ngauth_batch_crypt_pub.pem /etc/
ADD docker-files/ngbauth-submit /etc/sysconfig/
ADD docker-files/myschedd.yaml /etc/myschedd/

ADD docker-files/krb5.conf /etc/
RUN ln -s /etc/krb5.conf /etc/krb5.conf.no_rdns

RUN conda install --yes --freeze-installed \
    -c conda-forge \
    conda-build \
    python-blosc \
    cytoolz \
    "bokeh>=3.1.0" \
    dask==2024.9.0 \
    distributed==2024.9.0 \
    dask-gateway \
    dask-ml \
    dask-jobqueue==0.8.2 \
    && conda install --yes --freeze-installed \
    -c conda-forge \
    nomkl \
    scipy \
    tini \
    jupyter-server-proxy \
    htcondor \
    jupyterlab \
    ipympl \
    dask_labextension \
    python==3.11.* \
    "uproot>=4.3.7" \
    vector xgboost \
    lz4 python-xxhash zstandard \
    skorch \
    && conda install --yes --freeze-installed \
    -c conda-forge \
    coffea \
    voms \
    xrootd \
    gfal2 \
    gfal2-util \
    python-gfal2 \
    ca-policy-lcg \
    workflows \
    && pip3 install orion \
    && pip3 install p_tqdm \
    && conda clean --all -f -y \
    && conda build purge-all

# note that we are using the conda condor
RUN mkdir /usr/local/etc/condor/config.d
ADD docker-files/condor_submit.config /usr/local/etc/condor/config.d/

# but config still looked for in /etc/condor for some reason
RUN mkdir -p /etc/condor/
ADD docker-files/condor_config /etc/condor/
RUN mkdir -p /etc/condor/config.d
ADD docker-files/condor_submit.config /etc/condor/config.d/

RUN mkdir -p /root/pyinstall/dask_lxplus
ADD setup.py /root/pyinstall/
ADD setup.cfg /root/pyinstall/
ADD dask_lxplus/__init__.py /root/pyinstall/dask_lxplus/
ADD dask_lxplus/cluster.py /root/pyinstall/dask_lxplus/
ADD dask_lxplus/config.py /root/pyinstall/dask_lxplus/
ADD dask_lxplus/jobqueue-cern.yaml /root/pyinstall/dask_lxplus/
RUN chmod 0544 /root/pyinstall/setup.py
WORKDIR /root/pyinstall
RUN /root/pyinstall/setup.py build
RUN /root/pyinstall/setup.py install
RUN rm -rf /root/pyinstall

RUN ln -s /usr/local/etc/grid-security /etc/grid-security

ENTRYPOINT ["tini", "-g", "--"]
